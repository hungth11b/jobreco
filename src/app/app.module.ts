import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';
import appRoutes from './routerConfig';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { HomeComponent } from './pages/home/home.component';
import { RecoCreateComponent } from './pages/admin/users/reco/reco-create/reco-create.component';
import { RecoEditComponent } from './pages/admin/users/reco/reco-edit/reco-edit.component';
import { RecoIndexComponent } from './pages/admin/users/reco/reco-index/reco-index.component';
import { PartnerCreateComponent } from './pages/admin/users/partner/partner-create/partner-create.component';
import { PartnerEditComponent } from './pages/admin/users/partner/partner-edit/partner-edit.component';
import { PartnerIndexComponent } from './pages/admin/users/partner/partner-index/partner-index.component';
import { UserGroupCreateComponent } from './pages/admin/user-group/user-group-create/user-group-create.component';
import { UserGroupEditComponent } from './pages/admin/user-group/user-group-edit/user-group-edit.component';
import { UserGroupIndexComponent } from './pages/admin/user-group/user-group-index/user-group-index.component';
import { PositionsComponent } from './pages/admin/categories/positions/positions.component';
import { SkillsComponent } from './pages/admin/categories/skills/skills.component';
import { KeyJobsComponent } from './pages/admin/categories/key-jobs/key-jobs.component';
import { CountriesComponent } from './pages/admin/categories/countries/countries.component';
import { SlidesComponent } from './pages/admin/categories/slides/slides.component';
import { MenusComponent } from './pages/admin/categories/menus/menus.component';
import { RequestCreateComponent } from './pages/admin/requests/request-create/request-create.component';
import { RequestEditComponent } from './pages/admin/requests/request-edit/request-edit.component';
import { RequestIndexComponent } from './pages/admin/requests/request-index/request-index.component';
import { CandidateIndexComponent } from './pages/admin/candidates/candidate-index/candidate-index.component';
import { CandidateCreateComponent } from './pages/admin/candidates/candidate-create/candidate-create.component';
import { CandidateEditComponent } from './pages/admin/candidates/candidate-edit/candidate-edit.component';
import { ReportTotalComponent } from './pages/admin/report-total/report-total.component';
import { PaymentPartnerComponent } from './pages/admin/payment-partner/payment-partner.component';
import { PaymentCustomerComponent } from './pages/admin/payment-customer/payment-customer.component';
import { CustomerCreateComponent } from './pages/admin/customers/customer-create/customer-create.component';
import { CustomerEditComponent } from './pages/admin/customers/customer-edit/customer-edit.component';
import { CustomerIndexComponent } from './pages/admin/customers/customer-index/customer-index.component';
import { ApplicationIndexComponent } from './pages/admin/applications/application-index/application-index.component';
import { ApplicationEditComponent } from './pages/admin/applications/application-edit/application-edit.component';
import { ApplicationCreateComponent } from './pages/admin/applications/application-create/application-create.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CasesComponent } from './pages/admin/cases/cases.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    RecoCreateComponent,
    RecoEditComponent,
    RecoIndexComponent,
    PartnerCreateComponent,
    PartnerEditComponent,
    PartnerIndexComponent,
    UserGroupCreateComponent,
    UserGroupEditComponent,
    UserGroupIndexComponent,
    PositionsComponent,
    SkillsComponent,
    KeyJobsComponent,
    CountriesComponent,
    SlidesComponent,
    MenusComponent,
    RequestCreateComponent,
    RequestEditComponent,
    RequestIndexComponent,
    CandidateIndexComponent,
    CandidateCreateComponent,
    CandidateEditComponent,
    ReportTotalComponent,
    PaymentPartnerComponent,
    PaymentCustomerComponent,
    CustomerCreateComponent,
    CustomerEditComponent,
    CustomerIndexComponent,
    ApplicationIndexComponent,
    ApplicationEditComponent,
    ApplicationCreateComponent,
    PageNotFoundComponent,
    HomeComponent,
    CasesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,

    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
