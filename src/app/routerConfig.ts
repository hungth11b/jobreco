import { Routes } from '@angular/router';

import { RecoIndexComponent } from './pages/admin/users/reco/reco-index/reco-index.component';
import { PartnerIndexComponent } from './pages/admin/users/partner/partner-index/partner-index.component';
import { UserGroupIndexComponent } from './pages/admin/user-group/user-group-index/user-group-index.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HomeComponent } from './pages/home/home.component';
import { CasesComponent } from './pages/admin/cases/cases.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'acount-reco', component: RecoIndexComponent },
  { path: 'acount-partner', component: PartnerIndexComponent },
  { path: 'user-group', component: UserGroupIndexComponent },
  { path: 'cases', component: CasesComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];
export default appRoutes;