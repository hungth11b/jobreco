import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyJobsComponent } from './key-jobs.component';

describe('KeyJobsComponent', () => {
  let component: KeyJobsComponent;
  let fixture: ComponentFixture<KeyJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
