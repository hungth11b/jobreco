import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGroupIndexComponent } from './user-group-index.component';

describe('UserGroupIndexComponent', () => {
  let component: UserGroupIndexComponent;
  let fixture: ComponentFixture<UserGroupIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGroupIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGroupIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
