import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoCreateComponent } from './reco-create.component';

describe('RecoCreateComponent', () => {
  let component: RecoCreateComponent;
  let fixture: ComponentFixture<RecoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
