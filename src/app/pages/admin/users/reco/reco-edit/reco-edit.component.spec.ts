import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoEditComponent } from './reco-edit.component';

describe('RecoEditComponent', () => {
  let component: RecoEditComponent;
  let fixture: ComponentFixture<RecoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
