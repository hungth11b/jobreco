import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoIndexComponent } from './reco-index.component';

describe('RecoIndexComponent', () => {
  let component: RecoIndexComponent;
  let fixture: ComponentFixture<RecoIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
